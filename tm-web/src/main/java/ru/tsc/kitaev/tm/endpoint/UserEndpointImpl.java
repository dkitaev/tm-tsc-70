package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.api.endpoint.UserEndpoint;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@WebService(endpointInterface = "ru.tsc.kitaev.tm.api.endpoint.UserEndpoint")
public class UserEndpointImpl implements UserEndpoint {

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @Override
    @NotNull
    @WebMethod
    @PostMapping("/add")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public UserDTO add(
            @WebParam(name = "user", partName = "user")
            @NotNull @RequestBody final UserDTO user
    ) {
        return userService.add(user);
    }

    @Override
    @NotNull
    @WebMethod
    @PutMapping("/save")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public UserDTO save(
            @WebParam(name = "user", partName = "user")
            @NotNull @RequestBody final UserDTO user
    ) {
        return userService.update(user);
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<UserDTO> findAll() {
        return userService.findAll();
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public UserDTO findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    ) {
        return userService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @Nullable @PathVariable(value = "id") final String id
    ) {
        return userService.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public int getSize() {
        return userService.getSize();
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void clear() {
        userService.clear();
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(
            @WebParam(name = "user", partName = "user")
            @NotNull @RequestBody final UserDTO user
    ) {
        userService.delete(user);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    ) {
        userService.deleteById(id);
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findByLogin/{login}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public UserDTO findByLogin(
            @WebParam(name = "login", partName = "login")
            @NotNull @PathVariable(value = "login") final String login
    ) {
        return userService.findByLogin(login);
    }

}
