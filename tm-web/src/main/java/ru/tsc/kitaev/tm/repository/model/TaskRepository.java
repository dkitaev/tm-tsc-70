package ru.tsc.kitaev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractOwnerRepository<Task> {

    @Nullable
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
