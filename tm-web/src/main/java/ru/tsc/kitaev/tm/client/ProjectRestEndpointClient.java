package ru.tsc.kitaev.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.dto.ProjectDTO;

import java.util.List;

public interface ProjectRestEndpointClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/projects";

    static ProjectRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @NotNull
    @PostMapping("/add")
    ProjectDTO add(@NotNull @RequestBody final ProjectDTO project);

    @NotNull
    @PutMapping("/save")
    ProjectDTO save(@NotNull @RequestBody final ProjectDTO project);

    @Nullable
    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @Nullable
    @GetMapping("/findById/{id}")
    ProjectDTO findById(@NotNull @PathVariable(value = "id") final String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@Nullable @PathVariable(value = "id") final String id);

    @GetMapping("/count")
    int getSize();

    @DeleteMapping("/clear")
    void clear();

    @DeleteMapping("/delete")
    void delete(@NotNull @RequestBody final ProjectDTO project);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@NotNull @PathVariable(value = "id") final String id);


}
