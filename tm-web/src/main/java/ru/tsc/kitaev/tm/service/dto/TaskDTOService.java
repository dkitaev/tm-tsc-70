package ru.tsc.kitaev.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIdException;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.repository.dto.TaskDTORepository;

import java.util.List;

@Service
@NoArgsConstructor
public class TaskDTOService extends AbstractOwnerDTOService<TaskDTO> implements ITaskDTOService {

    @NotNull
    @Autowired
    private TaskDTORepository repository;

    @Override
    @Nullable
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

}
