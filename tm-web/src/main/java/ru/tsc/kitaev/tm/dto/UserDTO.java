package ru.tsc.kitaev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.enumerated.RoleType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "users")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserDTO extends AbstractEntityDTO {

    private String login;

    private String passwordHash;

    @NotNull
    private RoleType role = RoleType.USER;

}
