package ru.tsc.kitaev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.dto.AbstractEntityDTO;

import java.util.List;

public interface IDTOService<E extends AbstractEntityDTO> {

    void clear();

    List<E> findAll();

    E findById(@NotNull final String id);

    void removeById(@NotNull final String id);

    void addAll(@NotNull final List<E> entities);

    int getSize();

}
