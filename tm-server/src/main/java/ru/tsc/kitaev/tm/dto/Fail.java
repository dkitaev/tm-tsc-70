package ru.tsc.kitaev.tm.dto;

import org.jetbrains.annotations.NotNull;

public class Fail extends Result {

    public Fail() {
        success = false;
        message = "";
    }

    public Fail(final @NotNull Exception e) {
        success = false;
        message = e.getMessage();
    }

}
