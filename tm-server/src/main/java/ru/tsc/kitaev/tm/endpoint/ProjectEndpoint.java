package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.kitaev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kitaev.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<ProjectDTO> findProjectAll(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AbstractException {
        sessionService.validate(session);
        return projectService.findAll(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<ProjectDTO> findProjectAllSorted(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "sort", partName = "sort") @Nullable final String sort
    ) throws AbstractException {
        sessionService.validate(session);
        return projectService.findAll(session.getUserId(), sort);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        return projectService.findById(session.getUserId(), id);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectDTO findProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        sessionService.validate(session);
        return projectService.findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectDTO createProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws AbstractException {
        sessionService.validate(session);
        return projectService.create(session.getUserId(), name, description);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        sessionService.validate(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    public void startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public void changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        sessionService.validate(session);
        projectService.changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public boolean existsProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        return projectService.existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") final int index
    ) throws AbstractException {
        sessionService.validate(session);
        return projectService.existsByIndex(session.getUserId(), index);
    }

}
