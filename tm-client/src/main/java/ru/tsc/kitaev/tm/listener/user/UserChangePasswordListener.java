package ru.tsc.kitaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.listener.AbstractListener;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public class UserChangePasswordListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "change-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "change user password...";
    }

    @Override
    @EventListener(condition = "@userChangePasswordListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD");
        @NotNull final String password = TerminalUtil.nextLine();
        userEndpoint.setPassword(sessionService.getSession(), password);
        System.out.println("[OK]");
    }

}
