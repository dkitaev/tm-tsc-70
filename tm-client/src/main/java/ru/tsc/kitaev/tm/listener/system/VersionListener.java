package ru.tsc.kitaev.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;

@Component
public final class VersionListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String command() {
        return "version";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version...";
    }

    @Override
    @EventListener(condition = "@versionListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        System.out.println(propertyService.getApplicationVersion());
        System.out.println(Manifests.read("build"));
    }

}
