package ru.tsc.kitaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.endpoint.TaskDTO;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowAllFromProjectByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "tasks-show-by-project-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks by project id...";
    }

    @Override
    @EventListener(condition = "@taskShowAllFromProjectByIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final List<TaskDTO> taskUpdated = projectTaskEndpoint.findTaskByProjectId(sessionService.getSession(), projectId);
        if (taskUpdated == null) throw new TaskNotFoundException();
        System.out.println(taskUpdated);
    }

}
